package com.spark.match;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class MatchConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MatchConfigServerApplication.class, args);
	}

}

